
//3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
let num = 7;
let getCube = num ** 3;

console.log();
//4. Using Template Literals, print out the value of the getCube varaible with a mewssage of The cube of <num> is...

console.log(`The cube of ${num} is ${getCube}`);


//5. Create a variable address with a value of an array containing details of an address.

let address = [`17 Meadow Pl`, `Tustin`,`CA`, 92782]

//6. Destructure the array and print out a message with the full address using Template Literals.

let [street, city, state, zipcode] = address

console.log(`I live at ${street} ${city}, ${state} ${zipcode}. Come visit me~`);

//7. Create a variable animal with a value of an object data type with different animal details as its properties.

let animal = {
	type: `Fire-type`,
	region: `Hoenn`,
	height: 40.6
}

//8. Destructure the object and print out a message with the details of the animal using Template Literals.

const {type, region, height} = animal

console.log(`Torchic is a ${type} starter Pokemon of the ${region} region with an average height of ${height}cm.`);

//9. Create an array of numbers.

const setNum = [5, 10, 15, 20, 25]

//10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

setNum.forEach((setNum) => {console.log(`${setNum}`)})

//11. Create a class of a Dog and a constructor that will accept a name, age and breed as it's properties.

class Dog {
	constructor (name, age, breed) {
		this.name = name,
		this.breed = breed,
		this.age = age
	}
}

//12. Create/instantiate a new object from the class Dog and console log the object

const myDog = new Dog()
myDog.name = `Ashwynn`
myDog.breed = `Shih Tzu`
myDog.age = 13

console.log(myDog);






